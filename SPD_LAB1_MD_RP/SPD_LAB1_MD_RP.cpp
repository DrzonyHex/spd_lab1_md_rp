// SPD_LAB1_MD_RP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

//using namespace std;

int AmountMachines;
int AmountJobs;

class Job {

public:
	int id;
	int fullTime; //time needed to finish only this job by all machines
	std::vector <int> timeOnMachine; //vector of times to finish job on specify machines
										//number od vector object = number of machine

	Job(int job_id, int job_fullTime)
		:id(job_id), fullTime(job_fullTime)
	{
	}

};


bool loadFile(std::vector<Job> &JobVector) //returns filled vector
{
	std::string name;
	std::cout << "Podaj nazwe pliku: \n";
	std::cin >> name;
	name += ".DAT";

	std::ifstream file;
	file.open(name.c_str());
	if (file.good())
	{
		std::cout << "Udalo sie otworzyc plik" << std::endl;
		file >> AmountJobs >> AmountMachines;

		for (int j = 0; j < AmountJobs; j++)
		{
			JobVector.emplace_back(j, 0); //declaring elements of vector of Jobs, (size = amount of jobs)
			for (int i = 0; i < AmountMachines; i++)
			{
				JobVector[j].timeOnMachine.emplace_back();//declaring elementes of machine vector inside Job;
				file >> JobVector[j].timeOnMachine[i]; //loading from file to vector
				JobVector[j].fullTime += JobVector[j].timeOnMachine[i]; //filling the fulltime
			}
		}	
	}
	else
		{
			std::cout << "Nie udalo sie wczytac pliku" << std::endl;
			return false;
		}
}

void ViewVector(std::vector<Job> vector)             //wyswietlanie wczytanych danych
{
	std::cout << "Number of jobs: "<< AmountJobs << "\n" << "Number of machines: " << AmountMachines << "\n";
	for (int i = 0; i < AmountJobs; i++) {
		std::cout << "\n";
		for (int j = 0; j < AmountMachines; j++)
		{
			std::cout << vector[i].timeOnMachine[j] << " ";
		}
	}
	std::cout << "\n Order of jobs: " << "\n";
	for (int i = 0; i < AmountJobs; i++) 
	{
		std::cout << vector[i].id<<", ";
	}
	std::cout << "\n\n";
}

void SortVector(std::vector<Job> &vector) //Sorting by fulltime (from the longest do shortest)
{
	for (int j = 0; j < AmountJobs; j++) 
	{	
		sort(vector.begin(), vector.end(), [](Job a, Job b) -> int {return a.fullTime > b.fullTime; });
	}
//wyswietlanie
//	std::cout << "\n";
	//for (auto i : vector)
		//std::cout << i.fullTime << " " << i.id << "\n";
}

int Cmax(std::vector<Job> jobVector, int size)  //size - the amount of elements of vector we want to use
{
	std::vector<int> timeOnMachines;
	for (int i = 0; i < AmountMachines; ++i) 
	{
		timeOnMachines.push_back(0);
	}

	for (int job=0; job<size; job++)
	{
		timeOnMachines[0] += jobVector[job].timeOnMachine[0]; 
		for (int i = 0; i < AmountMachines - 1; i++) 
			timeOnMachines[i + 1] = std::max(timeOnMachines[i], timeOnMachines[i + 1]) + jobVector[job].timeOnMachine[i + 1];
	}

	return timeOnMachines[AmountMachines - 1];

}


int Permutation(std::vector<Job> &jobVector)
{ 
	int indeks;
	int min;
	std::vector<Job> buffjobVector;
	std::vector<Job> iterator;

	for (int i = 0; i < 2; i++) //adding first two elements for vector of jobs
	{
		buffjobVector.emplace_back(jobVector[i].id, jobVector[i].fullTime);
		for (int j = 0; j < AmountMachines; j++)
		{
			buffjobVector[i].timeOnMachine.emplace_back(jobVector[i].timeOnMachine[j]);//declaring elementes of machine vector inside Job;
		}
	}
	min = Cmax(buffjobVector, 2);
	std::swap(buffjobVector[0], buffjobVector[1]);
	if (Cmax(buffjobVector, 2) < min)
	{
		min = Cmax(buffjobVector, 2);
	}
	else
	{
		std::swap(buffjobVector[0], buffjobVector[1]);
	}
	int zz = 0;
	for (int j = 2; j < AmountJobs; j++)
	{
		size_t buforsize = buffjobVector.size();
		for (size_t z = 0; z <= buforsize; z++)
		{
	
			buffjobVector.insert((buffjobVector.begin()) + zz , jobVector[j]);
			//std::cout << "\n" << "obrot" << zz << "\n";
			//std::cout<<"\n";
			if (zz == 0)
			{
				min = Cmax(buffjobVector, buffjobVector.size());
				indeks = zz;
			}
			else
			{
				if (Cmax(buffjobVector, buffjobVector.size()) < min)
				{
					min = Cmax(buffjobVector, buffjobVector.size());
					indeks = zz;
				}

			}
			buffjobVector.erase((buffjobVector.begin()) + zz);
			zz++;
		}
		zz = 0;
		buffjobVector.insert((buffjobVector.begin()) + indeks, jobVector[j]);

	}
	std::cout << "After using algorithm: \n";
	ViewVector(buffjobVector);
	return min;
}


int main()
{	
	std::vector<Job> MainVector;
	if (loadFile(MainVector))
	{
		ViewVector(MainVector);
		SortVector(MainVector);
		std::cout << "\n" << "Optimal time by NEH algorithm: " << Permutation(MainVector) << "\n";
	}
	//std::cout<<"\n"<<Cmax(MainVector, MainVector.size())<<"\n";
	
	system("pause");
}

